package ph.apper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Service
public class UserService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserService.class);

    private final List<User> users = new ArrayList<>();
    private final Map<String, String> verificationCodes = new HashMap<>();
    private final IdService idService;

    public UserService(IdService idService) {
        this.idService = idService;
    }

    public User register(UserRegistration userRegistration) throws InvalidUserRegistrationException {
        // validate inputs. all fields are required. Valid email. age should be at least 18 years.
        if (userRegistration.getEmail().isBlank()
                || userRegistration.getFirstName().isBlank()
                || userRegistration.getLastName().isBlank()
                || userRegistration.getPassword().isEmpty()
                || Objects.isNull(userRegistration.getBirthDate())
        ) {
            throw new InvalidUserRegistrationException("All fields are required");
        }

        if (!userRegistration.getEmail().contains("@")) {
            throw new InvalidUserRegistrationException("Invalid email");
        }

        if (userRegistration.getPassword().length() < 5) {
            throw new InvalidUserRegistrationException("password must be at least 5 characters");
        }

        Period periodDiff = Period.between(userRegistration.getBirthDate(), LocalDate.now());
        if (periodDiff.getYears() < 18) {
            throw new InvalidUserRegistrationException("age must be at least 18");
        }

        // get user id
        String userId = idService.getNextUserId();
        LOGGER.info("Generated User ID: {}", userId);

        // save registration details as User with ID
        User newUser = new User(userId);
        newUser.setFirstName(userRegistration.getFirstName());
        newUser.setLastName(userRegistration.getLastName());
        newUser.setBirthDate(userRegistration.getBirthDate());
        newUser.setEmail(userRegistration.getEmail());
        newUser.setPassword(userRegistration.getPassword());

        // create verification code
        String verificationCode = idService.generateCode(5);

        verificationCodes.put(newUser.getEmail(), verificationCode);
        users.add(newUser);

        return newUser;
    }

    public int getNumberOfRegisteredUser() {
        return users.size();
    }

    public int getNumberOfVerificationCodes() {
        return verificationCodes.size();
    }
}
